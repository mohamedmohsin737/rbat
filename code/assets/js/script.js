/* Menu */
$( document ).ready(function() {
    var level2Link = $('.hbku-menu .nav-item');
    var level3Link = $('.hbku-menu .nav-item .nav-dropdown li ');
    var Responsive_Size = 1023;

    level2Link.attr("aria-expanded","false");
    level3Link.attr("aria-expanded","false");
    
    //if ($(window).width() <= Responsive_Size) {
        $(level2Link).on("click", function () {
            var navDD = $('.hbku-menu .nav-item > .nav-dropdown');
            navDD.addClass('showSubMenu');
            $(this).siblings().find('.nav-dropdown').removeClass('showSubMenu');
            $(this).attr("aria-expanded","true");
            $(this).siblings().attr("aria-expanded","false");
        });
        $(level3Link).on("click", function () {
             $(this).find('.nav-dropdown').addClass('showSubMenu');
             $(this).siblings().find('.nav-dropdown').removeClass('showSubMenu');
             $(this).attr("aria-expanded","true");
            $(this).siblings().attr("aria-expanded","false");
        });
   //}           
});

/* Carousel Banners */
$(document).ready(function () {
$('#owl-carousel4').owlCarousel({ 
           loop: true,
           items: 1
});


  $("#owl-carousel2").owlCarousel({
    loop:true,
    margin:15,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:2
        }
    }

  });
    $("#owl-carousel3").owlCarousel({
    loop:true,
    margin:15,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:2
        }
    }

  });

$("#directory-carousel").owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout: 5000,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dir-prev'),$('.dir-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#academic-carousel").owlCarousel({
    loop:true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep1-prev'),$('.dep1-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#research-carousel").owlCarousel({
    loop:true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep2-prev'),$('.dep2-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#admin-carousel").owlCarousel({
    loop:true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep3-prev'),$('.dep3-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#operations-carousel").owlCarousel({
    loop:true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep4-prev'),$('.dep4-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#office-carousel").owlCarousel({
    loop:false,
    center: true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep5-prev'),$('.dep5-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });
$("#press-carousel").owlCarousel({
    loop:false,
    center: true,
    margin:15,
    nav: true,
    dots: false,
    navText: [$('.dep6-prev'),$('.dep6-next')],
    responsive:{
        0:{
            items:1
        },
        1200:{
            items:4
        }
    }

  });


$("#serviceLinks").owlCarousel({
    loop:true,
    margin: 20,
    nav: false,
    dots: true,
    slideBy: "page",
    responsiveClass:true,
    responsive: {
      0: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      },
      768: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      },
      991: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      }
    }

  });

  var el = $("#owl-carousel1");
  var carousel;
  var carouselOptions = {
    loop:true,
    margin: 20,
    nav: false,
    dots: true,
    slideBy: "page",
    responsiveClass:true,
    responsive: {
      0: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      },
      768: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      },
      991: {
        items: 1,
        rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
      }
    }
  };

  //Taken from Owl Carousel so we calculate width the same way
  var viewport = function () {
    var width;
    if (
      carouselOptions.responsiveBaseElement &&
      carouselOptions.responsiveBaseElement !== window
    ) {
      width = $(carouselOptions.responsiveBaseElement).width();
    } else if (window.innerWidth) {
      width = window.innerWidth;
    } else if (
      document.documentElement &&
      document.documentElement.clientWidth
    ) {
      width = document.documentElement.clientWidth;
    } else {
      console.warn("Can not detect viewport width.");
    }
    return width;
  };

  var severalRows = false;
  var orderedBreakpoints = [];
  for (var breakpoint in carouselOptions.responsive) {
    if (carouselOptions.responsive[breakpoint].rows > 1) {
      severalRows = true;
    }
    orderedBreakpoints.push(parseInt(breakpoint));
  }

  //Custom logic is active if carousel is set up to have more than one row for some given window width
  if (severalRows) {
    orderedBreakpoints.sort(function (a, b) {
      return b - a;
    });
    var slides = el.find("[data-slide-index]");
    var slidesNb = slides.length;
    if (slidesNb > 0) {
      var rowsNb;
      var previousRowsNb = undefined;
      var colsNb;
      var previousColsNb = undefined;

      //Calculates number of rows and cols based on current window width
      var updateRowsColsNb = function () {
        var width = viewport();
        for (var i = 0; i < orderedBreakpoints.length; i++) {
          var breakpoint = orderedBreakpoints[i];
          if (width >= breakpoint || i == orderedBreakpoints.length - 1) {
            var breakpointSettings =
              carouselOptions.responsive["" + breakpoint];
            rowsNb = breakpointSettings.rows;
            colsNb = breakpointSettings.items;
            break;
          }
        }
      };

      var updateCarousel = function () {
        updateRowsColsNb();

        //Carousel is recalculated if and only if a change in number of columns/rows is requested
        if (rowsNb != previousRowsNb || colsNb != previousColsNb) {
          var reInit = false;
          if (carousel) {
            //Destroy existing carousel if any, and set html markup back to its initial state
            carousel.trigger("destroy.owl.carousel");
            carousel = undefined;
            slides = el.find("[data-slide-index]").detach().appendTo(el);
            el.find(".fake-col-wrapper").remove();
            reInit = true;
          }

          //This is the only real 'smart' part of the algorithm

          //First calculate the number of needed columns for the whole carousel
          var perPage = rowsNb * colsNb;
          var pageIndex = Math.floor(slidesNb / perPage);
          var fakeColsNb =
            pageIndex * colsNb +
            (slidesNb >= pageIndex * perPage + colsNb
              ? colsNb
              : slidesNb % colsNb);

          //Then populate with needed html markup
          var count = 0;
          for (var i = 0; i < fakeColsNb; i++) {
            //For each column, create a new wrapper div
            var fakeCol = $('<div class="fake-col-wrapper"></div>').appendTo(
              el
            );
            for (var j = 0; j < rowsNb; j++) {
              //For each row in said column, calculate which slide should be present
              var index =
                Math.floor(count / perPage) * perPage +
                (i % colsNb) +
                j * colsNb;
              if (index < slidesNb) {
                //If said slide exists, move it under wrapper div
                slides
                  .filter("[data-slide-index=" + index + "]")
                  .detach()
                  .appendTo(fakeCol);
              }
              count++;
            }
          }
          //end of 'smart' part

          previousRowsNb = rowsNb;
          previousColsNb = colsNb;

          if (reInit) {
            //re-init carousel with new markup
            carousel = el.owlCarousel(carouselOptions);
          }
        }
      };

      //Trigger possible update when window size changes
      $(window).on("resize", updateCarousel);

      //We need to execute the algorithm once before first init in any case
      updateCarousel();
    }
  }


  //init
  carousel = el.owlCarousel(carouselOptions);
});


